$(document).ready(function(){

    const gCOLUMN_ID = 0;
    const gCOLUMN_KICH_CO = 1;
    const gCOLUMN_LOAI_PIZZA = 2;
    const gCOLUMN_NUOC_UONG = 3;
    const gCOLUMN_THANH_TIEN = 4 ;
    const gCOLUMN_FULLNAME = 5;
    const gCOLUMN_PHONE = 6;
    const gCOLUMN_STATUS = 7;
    const gCOLUMN_ACTION = 8;
    
    var gPizzaSize = [  {"kichCo": "S", "duongKinh": 20, "suon": 2 , "salad": 200 , "soLuongNuoc": 2 , "thanhTien": 150000 } ,
                        {"kichCo": "M", "duongKinh": 25, "suon": 4 , "salad": 300 , "soLuongNuoc": 3 , "thanhTien": 200000 } , 
                        {"kichCo": "L", "duongKinh": 30, "suon": 8 , "salad": 500 , "soLuongNuoc": 4 , "thanhTien": 250000 } ];
    var gDataCreate = [];   

    var gId =  "";
    var gId1 = "";

    var vObjectRequest = {
        trangThai: "confirmed" //3 trang thai open, confirmed, cancel
    } 
    var vObjectRequest1 = {
            trangThai: "cancel" //3 trang thai open, confirmed, cancel
    } 

    var gDrink = [];

    const gCOL_NAME = ["orderId","kichCo" , "loaiPizza","idLoaiNuocUong","thanhTien","hoTen","soDienThoai","trangThai","action"]

    

    var gUser = {
        users:[] ,
        filterUser : function(paramFilter){
            var vResul = [] ;
            vResul = this.users.filter(function(user){
          // đây là cách lọc chỉ tương đối và là live nên chỉ cần nhập 1 vài chữ là sẽ ra tên luôn
          return ((paramFilter.trangThai == "" || user.trangThai.toUpperCase().includes(paramFilter.trangThai.toUpperCase()))
          &&      (paramFilter.loaiPizza == "" || user.loaiPizza.toUpperCase().includes(paramFilter.loaiPizza.toUpperCase()))) ;
        })
        return vResul
        }
    }
    var gUserTable = $("#user-table").DataTable( {
      // Khai báo các cột của datatable
      "columns" : [
          { "data" : gCOL_NAME[gCOLUMN_ID] },
          { "data" : gCOL_NAME[gCOLUMN_KICH_CO] },
          { "data" : gCOL_NAME[gCOLUMN_LOAI_PIZZA] },
          { "data" : gCOL_NAME[gCOLUMN_NUOC_UONG] },
          { "data" : gCOL_NAME[gCOLUMN_THANH_TIEN] },
          { "data" : gCOL_NAME[gCOLUMN_FULLNAME] },
          { "data" : gCOL_NAME[gCOLUMN_PHONE] },
          { "data" : gCOL_NAME[gCOLUMN_STATUS] },
          { "data" : gCOL_NAME[gCOLUMN_ACTION] }
      ],
      // Ghi đè nội dung của cột action, chuyển thành button chi tiết
      "columnDefs":[ 
      {
          "targets": gCOLUMN_ACTION,
          "defaultContent": `<i class="far fa-edit " style= "color : green ; cursor: pointer;" title="Edit " ></i>  <i class="fas fa-trash-alt" style= "color : red ; cursor: pointer;" title="Delete"></i>`
      },
    ]
    });
    // VÙNG 1 KHAI BÁO , LOAD DỮ LIỆU , GỌI SERVER
    onPageLoading();
    function onPageLoading(){
        callApiToGetAllOrder();
        callApiToGetDrinkList();
    }

    function callApiToGetAllOrder(){
        $.ajax({
            async:false,
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "GET",
            dataType:'json',
        success:function(res){
            gUser.users = res ;
            loadDataToTable(gUser.users)
            console.log(gUser.users)
            },
        error:function(error){
            console.assert(error)
            }
        });
    }
    function callApiToGetDrinkList(){
        $.ajax({
            async:false,
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            type: "GET",
            dataType:'json',
        success:function(res){
            loadDataToSelectDrink(res);
            console.log(res);
            },
        error:function(error){
            console.assert(error);
            }
        });
    }
    function callApiToCreate(paramData){
        $.ajax({
            async:false,
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
            type: "POST",
            contentType:"application/json;charset=UTF-8",
            data:JSON.stringify(paramData),
        success:function(res){
            gDataCreate = res ;
            console.log(res)
            alert("tạo đơn hàng thành công")
            },
        error:function(error){
            console.assert(error)
            }
        });
    }
    function callApiToUpdateConfirm(){
        $.ajax({
            async:false,
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
            type: "PUT",
            contentType:"application/json;charset=UTF-8",
            data:JSON.stringify(vObjectRequest),
        success:function(res){
            console.log(res)
            },
        error:function(error){
            console.assert(error)
            }
        });
    }
    function callApiToUpdateCancel(){
        $.ajax({
            async:false,
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
            type: "PUT",
            contentType:"application/json;charset=UTF-8",
            data:JSON.stringify(vObjectRequest1),
        success:function(res){
            console.log(res)
            },
        error:function(error){
            console.assert(error)
            }
        });
    }
    function callApiToDelete(){
        $.ajax({
            async:false,
            url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId1,
            type: "DELETE",
            dataType:"json",
        success:function(res){
            console.log(res)
            },
        error:function(error){
            console.assert(error)
            }
        });
    }
    // VÙNG 2 CÁC NÚT ẤN ONCLICK
    // nút tạo đơn mới hiện ra modal
    $(document).on("click" ,"#btn-add-order",function(){
        $("#insert-order-modal").modal("show");
        // khóa input combo khi nhấn nút tạo
        $(".kickcocombo").prop("disabled" , true);
    });
    // nút filter (lọc) dữ liệu
    $(document).on("click" , "#btn-filter-order" , function(){
        filterUser();
    });
    // chọn combo cho pizza
    $(document).on("change" , "#select-create-kich-co" , function(){
        callCulatedForSelectCombo();
    });
    // nút confirm đơn mới 
    $(document).on("click" , "#btn-insert-order" , function(){
        onBtnCreate();
    })
    // nút edit
    $(document).on("click" , ".fa-edit" , function(){
        $("#edit-order-modal").modal("show");
        $(".block-all :input").prop("disabled" , true);   
        $("#inp-update-trang-thai").removeAttr("disabled") ;
        getInfoMationUpDateFromRow(this);
    })
    // nút xác nhận sửa 
    $(document).on("click" , "#btn-update-order" , function(){
        statusUpdate();
    })
    // nút delete
    $(document).on("click" , ".fa-trash-alt" , function(){
       $("#delete-modal").modal("show");
       getInfomationToDelete(this);
    })
    // nút confirm xóa
    $(document).on("click" , "#btn-confirm-delete-course" , function(){
        afterDelete();
    })
    // VÙNG 3 XỬ LÝ SỰ KIỆN
    // lấy dữ liệu thẻ select để filter
     function getSelectData(paramData){
        paramData.trangThai = $("#select-status option:selected").val()
        paramData.loaiPizza = $("#select-pizzatype option:selected").val()
    }
    // filter User
    function filterUser(){
        var vFilter = {
            trangThai:"",
            loaiPizza:""
        }

        getSelectData(vFilter)

      var filterUser = gUser.filterUser(vFilter);

      loadDataToTable(filterUser)
    }
    // xử lý sự kiện thẻ select kích cỡ 
    function callCulatedForSelectCombo(){
        var vPickSize = $("#select-create-kich-co option:selected").val()

        $(".kickcocombo").empty()

        var vResulCombo = getSizeComboPizza(vPickSize);

        console.log(vResulCombo);

        if(vResulCombo.length > 0 ){
            $("#inp-create-duong-kinh").val(vResulCombo[0])
            $("#inp-create-suon").val(vResulCombo[1]) 
            $("#inp-create-salad").val(vResulCombo[2]) 
            $("#inp-create-so-luong-nuoc").val(vResulCombo[3]) 
            $("#inp-create-thanh-tien").val(vResulCombo[4])
        }
        else{
            $(".kickcocombo").prop("disabled" , true)
        }
    }
    // tính toán kích cỡ khớp từng size combo
    function getSizeComboPizza(paramKichCo) {
        var vSizeFound = false;
        var vSizeResul = [  {duongKinh:0},
                            {suon:0},
                            {salad:0},
                            {soLuongNuoc:0},
                            {thanhTien:0}
                        ];
        var i = 0;
        while (!vSizeFound && i < gPizzaSize.length) {
            if (gPizzaSize[i].kichCo === paramKichCo) {
                vSizeFound = true;
                vSizeResul[0] = gPizzaSize[i].duongKinh;
                vSizeResul[1] = gPizzaSize[i].suon;
                vSizeResul[2] = gPizzaSize[i].salad;
                vSizeResul[3] = gPizzaSize[i].soLuongNuoc;
                vSizeResul[4] = gPizzaSize[i].thanhTien;
            }
            else {
              i ++;
            }
        }
        return vSizeResul;
      }
    // load dữ liệu vào thẻ select drink
    function loadDataToSelectDrink(paramDrinkObject){
        $.each(paramDrinkObject, function(i, item) {
            $("#select-id-loai-nuoc-uong").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        })
    }
    // lấy dữ liệu để create
    function getDataToCreate(paramData){
        paramData.kichCo = $("#select-create-kich-co option:selected").val();
        paramData.duongKinh = $("#inp-create-duong-kinh").val().trim();
        paramData.suon = $("#inp-create-suon").val().trim();
        paramData.salad = $("#inp-create-salad").val().trim();
        paramData.loaiPizza = $("#select-loai-pizza option:selected").val();
        paramData.idVourcher = $("#inp-create-id-voucher").val().trim();
        paramData.idLoaiNuocUong = $("#select-id-loai-nuoc-uong option:selected").val();
        paramData.soLuongNuoc = $("#inp-create-so-luong-nuoc").val().trim();
        paramData.hoTen = $("#inp-create-ho-ten").val().trim();
        paramData.thanhTien = $("#inp-create-thanh-tien").val().trim();
        paramData.email = $("#inp-create-email").val().trim();
        paramData.soDienThoai = $("#inp-create-so-dien-thoai").val().trim();
        paramData.diaChi = $("#inp-create-dia-chi").val().trim();
        paramData.loiNhan = $("#inp-create-loi-nhan").val().trim();
    }
    // check dữ liệu
    function checkDataToCreate(paramData){
        if (paramData.kichCo == "") {
            alert("Bạn cần chọn kích cỡ Pizza!!!")
            return false;
        }
        if (paramData.loaiPizza == "") {
            alert("Bạn cần chọn Loại Pizza!!!")
            return false;
        }
        if (paramData.idLoaiNuocUong == "") {
            alert("Bạn cần chọn Loại nước uống!!!")
            return false;
        }
        if (paramData.hoTen == "") {
            alert("Bạn cần nhập họ tên!!!")
            return false;
        }
        if (paramData.email.length > 0) {
            var vCheckEmail = validateEmail(paramData.email);
            if (vCheckEmail === false) {
                return false;
            }
        }
        if (paramData.soDienThoai == "") {
            alert("Bạn cần nhập số điện thoại!!!")
            return false;
        }
        if (paramData.diaChi == "") {
            alert("Bạn cần nhập địa chỉ!!!")
            return false;
        }

        return true;
    }
    // check mail
    function validateEmail(paramEmail) {
        "use strict";
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var vCheckEmail = true;
        if (!mailformat.test(paramEmail)) {
            alert("Email chưa hợp lệ");
            vCheckEmail = false;
        }
        return vCheckEmail;
    }
    // xử lý dữ liệu nút confirm tạo đơn hàng
    function onBtnCreate(){
        var vObjectRequest = {
            kichCo: "",
            duongKinh: 0,
            suon: 0,
            salad: 0,
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: 0,
            hoTen: "",
            thanhTien: 0,
            email: "",
            soDienThoai: 0,
            diaChi: "",
            loiNhan: ""
        }    

        getDataToCreate(vObjectRequest);

        callApiToCreate(vObjectRequest);

        var vCheck = checkDataToCreate(vObjectRequest);

        if(vCheck == true){

            callApiToGetAllOrder();

            $("#insert-order-modal").modal("hide");
        }
    }
    // VÙNG 4 HÀM XÀI CHUNG CHO CẢ CHƯƠNG TRÌNH
    // hàm load dữ liệu toàn bộ 
    function loadDataToTable(paramResponseObject) {
        //Xóa toàn bộ dữ liệu đang có của bảng
        gUserTable.clear();
        //Cập nhật data cho bảng 
        gUserTable.rows.add(paramResponseObject);
        //Cập nhật lại giao diện hiển thị bảng
        gUserTable.draw();
    }
    function getInfoMationUpDateFromRow(paramButton){
        console.log("Nút Sửa được nhấn!");
        var vRowClick = $(paramButton).closest("tr"); // xác định tr chứa nút bấm được click
        var vTable = $("#user-table").DataTable(); // tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); 

        gId = vDataRow.id ; 
        gDrink = vDataRow.idLoaiNuocUong;

        $("#inp-update-kich-co").val(vDataRow.kichCo);
        $("#inp-update-duong-kinh").val(vDataRow.duongKinh);
        $("#inp-update-suon").val(vDataRow.suon);
        $("#inp-update-salad").val(vDataRow.salad);
        $("#inp-update-so-luong-nuoc").val(vDataRow.soLuongNuoc);
        $("#inp-update-thanh-tien").val(vDataRow.thanhTien);
        $("#select-update-loai-pizza").val(vDataRow.loaiPizza);
        $("#inp-update-id-voucher").val(vDataRow.idVourcher);
        $("#select-update-id-loai-nuoc-uong option:selected").text(gDrink);
        $("#inp-update-ho-ten").val(vDataRow.hoTen);
        $("#inp-update-email").val(vDataRow.email);
        $("#inp-update-so-dien-thoai").val(vDataRow.soDienThoai);
        $("#inp-update-dia-chi").val(vDataRow.diaChi);
        $("#inp-update-loi-nhan").val(vDataRow.loiNhan);
        $("#inp-update-trang-thai").val(vDataRow.trangThai);
        
      
    }
    function statusUpdate(){
        
        var vTrangThai = $("#inp-update-trang-thai option:selected").text()

        if (vTrangThai == "Confirmed"){
            callApiToUpdateConfirm();
            callApiToGetAllOrder();
            alert("Confirmed thành công");
            $("#edit-order-modal").modal("hide");
        }
        if (vTrangThai == "Cancel"){
            callApiToUpdateCancel();
            callApiToGetAllOrder();
            alert("Cancel thành công");
            $("#edit-order-modal").modal("hide");
        }
    }
    function getInfomationToDelete(paramButton){
        console.log("Nút Delete được nhấn!");
        var vRowClick = $(paramButton).closest("tr"); // xác định tr chứa nút bấm được click
        var vTable = $("#user-table").DataTable(); // tạo biến truy xuất đến DataTable
        var vDataRow = vTable.row(vRowClick).data(); 

        gId1 = vDataRow.id ; 

    }
    function afterDelete(){

        callApiToDelete();
        
        alert("xóa thành công");
        
        callApiToGetAllOrder();

        $("#delete-modal").modal("hide");
    }
})